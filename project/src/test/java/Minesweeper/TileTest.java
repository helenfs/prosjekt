package Minesweeper;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;	
	
public class TileTest {
	private Tile tile;

	@BeforeEach
	public void setup() {
		tile = new Tile(false);
	}
	
	@Test
	@DisplayName("Teste mine-attributt")
	public void testMineSettings() {
		Assertions.assertFalse(tile.isMine(), "Tile skal initialiseres uten mine");
		
		tile.setMine();
		
		Assertions.assertTrue(tile.isMine(), "Tile satt til mine skal ha mine");

	}
	
	@Test
	@DisplayName("Teste �pne-attributt")
	public void testOpenSettings() {
		Assertions.assertFalse(tile.isOpened(), "Tile kan ikke v�re �pen uten � ha blitt �pnet");
		
		tile.setOrRemoveFlag();
		tile.setOpened();
		Assertions.assertFalse( tile.isOpened(), "Tile kan ikke �pnes n�r den er flagget");
		
		tile.setOrRemoveFlag();
		tile.setOpened();
		Assertions.assertTrue(tile.isOpened(), "Tile skal kunne �pnes n�r flegget er fjernet");
	}
	
	@Test
	@DisplayName("Teste flagg-attributt")
	public void testFlaggSettings() {
		Assertions.assertFalse(tile.isFlagged(), "Tile kan ikke v�re flagget uten � ha blitt flagget");
		
		tile.setOrRemoveFlag();
		Assertions.assertTrue(tile.isFlagged(),"Tile skal v�re flagget n�r den er flagget");
		
		tile.setOrRemoveFlag();
		Assertions.assertFalse(tile.isFlagged(),"Tile skal ikke v�re flagget n�r flagget er fjernet");

	}
	
	@Test
	@DisplayName("Teste Game over")
	public void testGameOver() {
		Assertions.assertFalse(tile.checkIfGameOver(), "Spillet er ikke tapt f�r Tile er �pnet");
		
		tile.setMine();
		tile.setOpened();
		
		Assertions.assertTrue(tile.checkIfGameOver(), "Spillet er tapt n�r man �pner en mine");
	}
	
//	@Test
//	public void testSetValidType() {
//		tile.setType('a');
//		assertTrue(tile.isAir());
//		tile.setType('s');
//		assertTrue(tile.isSnake());
//		assertThrows(
//			IllegalArgumentException.class, 
//			() -> tile.setType('?'), 
//			"IllegalArgument skal kastes når man setter Tile til ugyldig verdi!"
//		);
//	}
//
//	@Test
//	public void testSetInvalidType() {
//		assertThrows(IllegalArgumentException.class, () -> {
//			tile.setType('?');
//		}, "IllegalArgument skal kastes når man setter Tile til ugyldig verdi!");
//	}
}

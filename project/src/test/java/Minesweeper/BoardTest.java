package Minesweeper;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BoardTest {
	private Board board;
	
	@BeforeEach
	public void setup() {
		int rowsAndCols = 10;
		int mines = 15;
		board = new Board(rowsAndCols, mines);
	}
	
	//Teste:
	//- antall miner/rader og kolonner samsvarer med initialisering
	//- unntak kastes om negative tall
	
	@Test
	@DisplayName("Teste for ugyldig oppsett")
	public void testIllegalBoard() {
		
//		Board board2 = new Board(-1, 3);
		Assertions.assertThrows(IllegalArgumentException.class, () -> new Board(-1, 3), "Det skal ikke v�re mulig � opprette et brett med mindre enn 1 rader og kolonner.");
		
		Assertions.assertThrows(IllegalArgumentException.class, () -> new Board(5, -3), "Det skal ikke v�re mulig � opprette et brett med negativt antall miner.");
	
		Assertions.assertThrows(IllegalArgumentException.class, () -> new Board(5, 30), "Det skal ikke v�re mulig � opprette et brett med flere miner enn Tiles.");

	}
	
	@Test
	@DisplayName("Teste samsvar")
	public void testBoardAttributes() {
		Assertions.assertEquals(10, board.getRowsAndCols(), "Antall rader og kolonner stemmer ikke overens med initialisering.");
		
		int numTiles = 100;
		Assertions.assertEquals(numTiles, board.getNumTiles(), "Antall ruter stemmer ikke overens med initialisering.");
		
		int numMines = 0;
		for (int i = 0; i < board.getRowsAndCols(); i++) {
			for (int j = 0; j < board.getRowsAndCols(); j++) {
				if (board.getTile(i, j).isMine()) {
					numMines++;
				}
			}
		}
		Assertions.assertEquals(numMines, board.getMines(), "Antall miner stemmer ikke overens med initialisering.");
		
	}
	
	@Test
	@DisplayName("Teste Tiles p� Board")
	public void testTileAttributes() {
		Board board2 = new Board(10, 0);
		Tile tile = new Tile(false);
		
		Assertions.assertEquals(tile.toString(), board2.getTile(0, 0).toString(), "Type mine stemmer ikke med initialisering av brettet.");
		
		tile.setOpened();
		board2.getTile(0, 0).setOpened();
		Assertions.assertEquals(tile.toString(), board2.getTile(0, 0).toString(), "Type mine stemmer ikke med initialisering av brettet.");

		
		Assertions.assertEquals(0, board2.getMinesAround(0, 0), "Feil telling av miner rundt Tile");
		
		board2.getTile(0, 1).setMine();
		board2.getTile(1, 0).setMine();
		
		Assertions.assertEquals(2, board2.getMinesAround(0, 0), "Feil telling av miner rundt Tile");

	}
	
	@Test
	@DisplayName("Teste seier og tap")
	public void testVictoryAndLoss() {
		Board board2 = new Board(5, 25);
		
		Assertions.assertFalse( board.checkVictory(), "Skal ikke v�re seier f�r kun miner er u�pnet");
		Assertions.assertTrue( board2.checkVictory(), "Skal v�re seier n�r kun miner er u�pnet");

		Assertions.assertFalse( board.checkGameOver(), "Skal ikk v�re tap f�r en mine er �pnet");
		
		board.getTile(0, 0).setMine();
		board.getTile(0, 0).setOpened();

		Assertions.assertTrue( board.checkGameOver(), "Skal v�re tap n�r en mine er �pnet");

	}
}

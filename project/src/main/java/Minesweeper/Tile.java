package Minesweeper;

public class Tile {
	private boolean isMine;
	private boolean isFlagged = false;
	private boolean isOpened = false;
	
	public Tile(boolean isMine){
		validateTile(isMine);
		this.isMine = isMine;
	}

	private void validateTile(boolean isMine) {
		if(isMine != true && isMine != false) {
			throw new IllegalArgumentException("Ugyldig initialisering av Tile");
		}
	}
	
	public boolean isMine() {
		return isMine;
	}

	public void setMine() {
		this.isMine = true;
	}
	
	public boolean isFlagged() {
		return isFlagged;
	}

	public void setOrRemoveFlag() {
		if(!this.isOpened()) {
			this.isFlagged = !this.isFlagged;
		}
			
	}

	public boolean isOpened() {
		return isOpened;
	}

	public void setOpened() {
		if(!this.isOpened && !this.isFlagged) {
			this.isOpened = true;
		}
	}
	
	public boolean checkIfGameOver() {
		return this.isOpened() && this.isMine();
	}
	
	@Override
	public String toString() {
		return this.isMine() + " " + this.isFlagged() + " " + this.isOpened();
	}
	
	public static void main(String[] args) {
		Tile t = new Tile(true);
		System.out.println(t.toString());
	}
}


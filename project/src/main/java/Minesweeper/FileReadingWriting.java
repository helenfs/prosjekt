package Minesweeper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileReadingWriting implements IMinesweeperFileReading {
	public final static String SAVE_GAME = "src/main/java/Minesweeper/";
	
	public FileReadingWriting() {
	}
	
	private static String getFilePath(String filename) {
		return SAVE_GAME + filename + ".txt";
	}
	
	@Override
	public void save(String filename, Board minesweeperGame){		// fjernet static
		try (PrintWriter writer = new PrintWriter(getFilePath(filename))) {
			writer.println(minesweeperGame.getRowsAndCols());
			
			for (int i = 0; i < minesweeperGame.getRowsAndCols(); i++) {
				for (int j = 0; j < minesweeperGame.getRowsAndCols(); j++) {
					writer.print(minesweeperGame.getTile(i, j).isMine() + ",");
					writer.print(minesweeperGame.getTile(i, j).isOpened() + ",");
					writer.println(minesweeperGame.getTile(i, j).isFlagged());
				}
			}
			writer.flush();
			writer.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Board getFromFile(String filename) throws FileNotFoundException{		
		Scanner scanner = new Scanner(new File(filename));
		
		int rowsAndCols = scanner.nextInt();
		
		Board board = new Board(rowsAndCols, 0);
		scanner.nextLine();
		
		while (scanner.hasNextLine()) {	
			
			for (int i = 0; i < board.getRowsAndCols(); i++) {
				for (int j = 0; j < board.getRowsAndCols(); j++) {
					Tile tile = board.getTile(i, j);	
					String line = scanner.nextLine();
					String[] lineInfo = line.split(",");
					
					if (lineInfo[0].equals("true")) {
						tile.setMine();
					}
					if (lineInfo[1].equals("true")) {
						tile.setOpened();
					}
					if (lineInfo[2].equals("true")) {
						tile.setOrRemoveFlag();
					}
				}
			}
			
			
		}
		scanner.close();
		return board;
	}
		
	
	public static void main(String[] args) {
		FileReadingWriting filereadingwriting = new FileReadingWriting();
//		Board board = new Board(5, 5);
//		System.out.println(board.printBoard());
//		filereadwrite.save("testBoard", board);
		
		try {
			Board board = filereadingwriting.getFromFile("src/main/java/Minesweeper/testBoard.txt");
			System.out.println(board.printBoard());

		} catch (FileNotFoundException e) {
//			e.printStackTrace();
			System.out.println("Fant ikke filen");
		}

		
	}
}

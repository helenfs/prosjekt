package Minesweeper;		//Denne filen kj�res for � kj�re spillet!

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MinesweeperApp extends Application{

	@Override
	public void start(final Stage primaryStage) throws Exception {
		Parent parent = FXMLLoader.load(getClass().getResource("Minesweeper.fxml"));
		primaryStage.setTitle("Minesweeper");
		primaryStage.setScene(new Scene(parent));
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		MinesweeperApp.launch(args);
	}

}

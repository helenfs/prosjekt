package Minesweeper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IMinesweeperFileReading {
	
	void save(String filename, Board minesweeperGame);
	
	Board getFromFile(String filename) throws FileNotFoundException;
	
//	void writeMinesweeperList(Board minesweeper, OutputStream os);
//	
//	void writeMinesweeperList(Board minesweeper) throws IOException;
//
//	Board readMinesweeperList(InputStream is);
//	
//	Board readMinesweeperList(String name) throws IOException;
}

package Minesweeper;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class Board {
	private int rowsAndCols;
	private int mines;
	private int numTiles;
	private List<Tile> tileArray; 
	private List<ArrayList<Tile>> board;
	
	public Board(int rowsAndCols, int mines) throws IllegalArgumentException {		//
		validateBoard(rowsAndCols, mines);
		
		this.rowsAndCols = rowsAndCols;
		this.mines = mines;
		this.tileArray = new ArrayList<>();
		
		this.numTiles = rowsAndCols*rowsAndCols;
		this.board = new ArrayList<ArrayList<Tile>>();
		
		addTiles(mines, true);
		addTiles(numTiles - mines, false); 
		
		Collections.shuffle(tileArray);
		
		for (int i = 0; i < rowsAndCols; i++) {
			ArrayList<Tile> row = new ArrayList<>();
			for (int j = 0; j < rowsAndCols; j++) {
				row.add(tileArray.get(i*rowsAndCols+j));
			}
			board.add(row);
		}
	}
	
	public void validateBoard(int rowsAndCols, int mines) throws IllegalArgumentException{	
		if(rowsAndCols <= 0) {
			throw new IllegalArgumentException("Kan ikke ha 0 eller f�rre Tile");
		}
		if(mines < 0) {
			throw new IllegalArgumentException("Kan ikke ha negativt antall miner");
		}
		if(mines > rowsAndCols*rowsAndCols) {
			throw new IllegalArgumentException("Kan ikke ha flere miner enn anntall ruter p� brettet");
		}
	}
	
	private void addTiles(int tiles, boolean isMines) {
		for (int i = 0; i < tiles; i++) {
			Tile t = new Tile(isMines);
			this.tileArray.add(t);
		}
	}
	
	public int getRowsAndCols() {
		return this.rowsAndCols;
	}
	
	public int getNumTiles() {
		return this.numTiles;
	}
	
	public int getMines() {
		return this.mines;
	}
	
	public Tile getTile(int row, int column) {
		return this.board.get(row).get(column);
	}
	
	public int getMinesAround(int row, int column) {
		if (this.getTile(row, column).isMine()) {
			throw new IllegalArgumentException("Kan ikke returnere antall miner rundt en mine.");
		}
		
		int minesAround = 0;
		for (int i = row - 1; i < row + 2; i++) {
			for (int j = column - 1; j < column + 2; j++) {
				if(i >= 0 && j >= 0 && i < this.getRowsAndCols() && j < this.getRowsAndCols()) {
					if (this.getTile(i, j).isMine()) {
						minesAround++;
					}
				}
			}
		}
		return minesAround;
	}
	
	public boolean checkVictory() {
		
		for (int i = 0; i < this.getRowsAndCols(); i++) {			//dette b�r flyttes til board.java
			for (int j = 0; j < this.getRowsAndCols(); j++) {
				if (!this.getTile(i, j).isOpened() && !this.getTile(i, j).isMine()) {		
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean checkGameOver() {
		
		for (int i = 0; i < this.getRowsAndCols(); i++) {			//dette b�r flyttes til board.java
			for (int j = 0; j < this.getRowsAndCols(); j++) {
				if (this.getTile(i, j).checkIfGameOver()) {		
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		String dispBoard = "";
		
		for (int i = 0; i < this.getRowsAndCols(); i++) {
			for (int j = 0; j < this.getRowsAndCols(); j++) {
				if (this.getTile(i, j).isFlagged()) {
					dispBoard += "f ";
				}
				else if (!this.getTile(i, j).isOpened()){
					dispBoard += "[] ";
				}
				
				else if (this.getTile(i, j).isOpened() && this.getTile(i, j).isMine()){
					dispBoard += " x ";
				}
				else {
					dispBoard += " " + this.getMinesAround(i, j) + " ";
				}
			}
			dispBoard += "\n";
		}
		
		return dispBoard;
	}
	
	public String printBoard() {
		String dispMines = "";
		
		for (int i = 0; i < this.getRowsAndCols(); i++) {
			for (int j = 0; j < this.getRowsAndCols(); j++) {
				if (this.getTile(i, j).isMine()) {
					dispMines += "x ";
				}
				else {
					dispMines += "0 ";
				}
			}
			dispMines += "\n";
		}
		
		return dispMines;
	}
	
	public static void main(String[] args) {
		Board b1 = new Board(4, 8);
		System.out.println(b1.toString());
		System.out.println(b1.printBoard());
		b1.getTile(3,  1).setOrRemoveFlag();
		b1.getTile(2, 0).setOpened();
		b1.getTile(1, 0).setOpened();
		b1.getTile(0, 0).setOpened();

		System.out.println(b1.toString());
		
		
		System.out.println(b1.numTiles);
		
//		Board board2 = new Board(10, 0);
//		Tile tile = new Tile(false);
//		Tile tile2 = board2.getTile(0, 0);
//		
//		System.out.println(tile.equals(tile2));
	
	}
	
}
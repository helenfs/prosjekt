package Minesweeper;

import java.io.FileNotFoundException;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class MinesweeperController {

	private FileReadingWriting fileReadWrite = new FileReadingWriting();
	private Board minesweeperBoard;
	@FXML private TextField rowsAndCols, mines;
	
	@FXML private Pane board;
	
	
	@FXML
	public void onStartGame() {		
		
		try {
			int rowsAndCols = Integer.parseInt(this.rowsAndCols.getText());
			int mines = Integer.parseInt(this.mines.getText());
			
			if(rowsAndCols > 14) {
				Text errorText = new Text();
				errorText.setText("Brettet kan ikke ha flere enn \n 14 rader og kolonner");
				errorText.setStyle("-fx-font-size: 40px");
				errorText.setFill(Color.RED);
				errorText.setTranslateX(16.0);
				errorText.setTranslateY(20.0);
				this.board.getChildren().add(errorText);
			}
			else {
				minesweeperBoard = new Board(rowsAndCols, mines);
				createBoard();
				drawBoard();
			}
			
		}
		
		catch (NumberFormatException e){
			System.out.println("Ugyldig input");
			Text errorText = new Text();
			errorText.setText("Ugyldig input");
			errorText.setStyle("-fx-font-size: 40px");
			errorText.setFill(Color.RED);
			errorText.setTranslateX(16.0);
			errorText.setTranslateY(20.0);
			this.board.getChildren().add(errorText);
		}
		catch (IllegalArgumentException  e) {
			System.out.println("Ugyldig input!!");
			Text errorText = new Text();
			errorText.setText("Ugyldig input!!");
			errorText.setStyle("-fx-font-size: 40px");
			errorText.setFill(Color.RED);
			errorText.setTranslateX(16.0);
			errorText.setTranslateY(20.0);
			this.board.getChildren().add(errorText);
		}		
		
	}
	
	@FXML
	public void getSavedGame() {
		try {
			this.minesweeperBoard = this.fileReadWrite.getFromFile("src/main/java/Minesweeper/testBoard.txt");
		} 
		catch (FileNotFoundException e) {
			System.out.println("Fant ikke filen");

		}
		createBoard();
		drawBoard();
	}
	
	@FXML
	public void saveGameProgress() {
		this.fileReadWrite.save("testBoard", this.minesweeperBoard);
				
	}
	
	private void createBoard() {
		board.getChildren().clear();
		
		for (int y = 0; y < minesweeperBoard.getRowsAndCols(); y++) {
			for (int x = 0; x < minesweeperBoard.getRowsAndCols(); x++) {
				int row = x;
				int col = y;
				Pane pane = new Pane();
				pane.setTranslateX(x*30);
				pane.setTranslateY(y*30);
				pane.setPrefWidth(30);
				pane.setPrefHeight(30);
				board.getChildren().add(pane);
				pane.setOnMouseClicked(e -> {
					if (e.getButton() == MouseButton.PRIMARY && !minesweeperBoard.getTile(row, col).isFlagged()) { // && ikke flagget
						primaryMouseClick(row, col);
					}
					if (e.getButton() == MouseButton.SECONDARY && !minesweeperBoard.getTile(row, col).isOpened()) {
						secondaryMouseClick(row, col);
					}
					drawBoard();
					Label text = new Label(getTileText(row, col));
					pane.getChildren().add(text);
					
				});
				Label text = new Label(getTileText(row, col));
				pane.getChildren().add(text);

				
			}
		}
		
		
	}
	
	private void drawBoard() {
		for (int y = 0; y < minesweeperBoard.getRowsAndCols(); y++) {
			for (int x = 0; x < minesweeperBoard.getRowsAndCols(); x++) {
				if (minesweeperBoard.getTile(x, y).isOpened()) {
					board.getChildren().get(y*minesweeperBoard.getRowsAndCols() + x).setStyle("-fx-background-color: " + getTileColor(minesweeperBoard.getTile(x, y)) + ";");
				}
				else {
					board.getChildren().get(y*minesweeperBoard.getRowsAndCols() + x).setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-background-color: " + getTileColor(minesweeperBoard.getTile(x, y)) + ";");

				}
			}
		}
		isVictory();
		isGameOver();
	}
	
	private void isVictory() {
		
		if(minesweeperBoard.checkVictory()) {
			Text winText = new Text();
			winText.setText("Du vant!");
			winText.setStyle("-fx-font-size: 40px");
			winText.setFill(Color.GREEN);
			winText.setTranslateX(16.0);
			winText.setTranslateY(20.0);
			this.board.getChildren().add(winText);
		} 
	}
	
	private void isGameOver() {
		
		if(minesweeperBoard.checkGameOver()) {
			Text loseText = new Text();
			loseText.setText("Game Over");
			loseText.setStyle("-fx-font-size: 40px");
			loseText.setFill(Color.RED);
			loseText.setTranslateX(16.0);
			loseText.setTranslateY(20.0);
			this.board.getChildren().add(loseText);
		}
	}
	
	private String getTileColor(Tile tile) {
		if(tile.isOpened()) {
			return "#aaaaaa";		
		}
		else if(tile.isFlagged()) {
			return "#ff0000";
		}
		else {
			return "#ADD8E6";
		}							
	}
	
	private void primaryMouseClick(int x, int y) {
		minesweeperBoard.getTile(x, y).setOpened();
		isVictory();
		isGameOver();
	}
	
	private void secondaryMouseClick(int x, int y) {
		minesweeperBoard.getTile(x, y).setOrRemoveFlag();
	}
	
	private String getTileText(int row, int column) {
		Tile tile = minesweeperBoard.getTile(row, column);
		
		if (tile.isOpened() && tile.isMine()){
			return "x";
		}
		else if (tile.isOpened() && !tile.isMine()){
			return String.valueOf(minesweeperBoard.getMinesAround(row, column));
		}
		else {
			return "";
		}
	}
	
	
}
